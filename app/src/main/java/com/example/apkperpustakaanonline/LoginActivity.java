package com.example.apkperpustakaanonline;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

public class LoginActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_login);

        String username_valid = "usm";
        String password_valid = "jaya";

        EditText edUsername = (EditText) findViewById(R.id.edUsername);
        EditText edPassword = (EditText) findViewById(R.id.edPassword);

        Button btnSubmit = (Button) findViewById(R.id.button1);

        btnSubmit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                if ((edUsername.getText().toString().trim().isEmpty()) || (edPassword.getText().toString().trim().isEmpty())) {
                    // JIKA KEDUA ATAU SALAH SATU ISIAN ADA YANG KOSONG
                    Toast.makeText(getApplicationContext(), "Isisan tidak valid", Toast.LENGTH_SHORT).show();
                } else {
                    // JIKA KEDUA ISIAN TELAH TERISI
                    if ((edUsername.getText().toString().trim().equals(username_valid)) && (edPassword.getText().toString().trim().equals(password_valid))) {
                        //Toast.makeText(getApplicationContext(), "Login berhasil", Toast.LENGTH_SHORT).show();

                        Intent intent_dasboard = new Intent(LoginActivity.this, MainActivity.class);
                        startActivity(intent_dasboard);
                        finish();

                    } else {
                        Toast.makeText(getApplicationContext(), "Login gagal", Toast.LENGTH_SHORT).show();
                    }
                }
            }
        });
    }
}